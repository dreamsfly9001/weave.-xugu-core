﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlSugar.Xugu
{
    public class XuguUpdateBuilder : UpdateBuilder
    {
        protected override string TomultipleSqlString(List<IGrouping<int, DbColumnInfo>> groupList)
        {
            if (groupList == null || groupList.Count == 0) return "SELECT 0 FROM DUAL";
            StringBuilder sb = new StringBuilder();
            int i = 0;
            sb.AppendLine(string.Join(UtilConstants.ReplaceCommaKey.Replace("{", "").Replace("}", ""), groupList.Select(t =>
            {
                var updateTable = string.Format("UPDATE {0} SET ", base.GetTableNameStringNoWith);
                var setValues = string.Join(",", t.Where(s => !s.IsPrimarykey).Select(m => GetOracleUpdateColums(i, m, false)).ToArray());
                var pkList = t.Where(s => s.IsPrimarykey).ToList();
                List<string> whereList = new List<string>();
                foreach (var item in pkList)
                {
                    var isFirst = pkList.First() == item;
                    var whereString = "";
                    whereString += GetOracleUpdateColums(i, item, true);
                    whereList.Add(whereString);
                }
                i++;
                return string.Format("{0} {1} WHERE {2} ", updateTable, setValues, string.Join(" AND", whereList));
            }).ToArray()));
            return sb.ToString();
        }
        private object GetValue(DbColumnInfo it) => FormatValue(it.Value);
        private string GetOracleUpdateColums(int i, DbColumnInfo m, bool iswhere) => string.Format("{0}={1}", m.DbColumnName, base.GetDbColumn(m, FormatValue(i, m.DbColumnName, m.Value, iswhere)));
        public object FormatValue(int i, string name, object value, bool iswhere)
        {
            if (value == null) return "NULL";
            else
            {
                var type = UtilMethods.GetUnderType(value.GetType());

                if (type == UtilConstants.DateType && iswhere == false) return GetParameterName(i, name, value);
                else if (value is DateTimeOffset) return GetParameterName(i, name, value);
                else if (type == UtilConstants.DateType && iswhere) return GetParameterName(i, name, value);
                else if (type.IsEnum())
                {
                    if (this.Context.CurrentConnectionConfig.MoreSettings?.TableEnumIsString == true) return GetParameterName(i, name, value.ToSqlValue());
                    else return GetParameterName(i, name, Convert.ToInt64(value));
                }
                else if (type == UtilConstants.ByteArrayType) return GetParameterName(i, name, value);
                else if (value is int || value is long || value is short || value is short || value is byte) return GetParameterName(i, name, value);
                else if (value is bool) return GetParameterName(i, name, value);
                else if (type == UtilConstants.StringType || type == UtilConstants.ObjType) return GetParameterName(i, name, value.ToString().ToSqlFilter());
                else return GetParameterName(i, name, value.ToString());
            }
        }
        private object GetParameterName(int i, string name, object value)
        {

            var parameterName = this.Builder.SqlParameterKeyWord + name + i;
            this.Parameters.Add(new SugarParameter(parameterName, value));
            return parameterName;
        }
    }
}
